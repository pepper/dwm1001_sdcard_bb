# DWM1001 Developement Board SDCARD Breakout

This Respository contains an SDCARD Breakout that interfaces with the Raspberry Pi
headers. It uses the SPI1 rapsberry pins as well as GPIO22 as the Card Detect Pin.

## Parts

- [HRS(Hirose) DM3CS-SF](https://www.hirose.com/en/product/p/CL0609-0032-3-00#)

## Pinout Mapping

|             |  RPi Pin Num.  | RPi Connector Sch. Name |   DWM1001 Pin Num  | nRF52832 Pin |
|:-----------:|:--------------:|:-----------------------:|:------------------:|:------------:|
| Card Detect |   15 (GPIO22)  |         GPIO_RPI        |   Pin 19 (READY)   |     P0.26    |
|    SPI CS   |  24 (SPI0 CS0) |          CS_RPI         |  Pin 29 (CPI_CSn)  |     P0.3     |
|   SPI CLK   | 23 (SPI0 SCLK) |         SPI1_CLK        |   Pin 25 (GPIO_8)  |     P0.4     |
|   SPI MOSI  | 21 (SPI0 MOSI) |        SPI1_MOSI        | Pin 27 (SPIS_MOSI) |     P0.6     |
|   SPI MISO  | 19 (SPI0 MISO) |        SPI1_MISO        | Pin 26 (SPIS_MISO) |     P0.7     |

If using RIOT the SD Card configuration macros are:

```c
/**
 * @name    SD Card device configuration
 * @{
 */
#define SDCARD_SPI_PARAM_SPI         SPI_DEV(0)
#define SDCARD_SPI_PARAM_CD          (GPIO_PIN(0, 26))
#define SDCARD_SPI_PARAM_CS          (GPIO_PIN(0, 3))
#define SDCARD_SPI_PARAM_CLK         (GPIO_PIN(0, 4))
#define SDCARD_SPI_PARAM_MOSI        (GPIO_PIN(0, 6))
#define SDCARD_SPI_PARAM_MISO        (GPIO_PIN(0, 7))
/** @} */
```

